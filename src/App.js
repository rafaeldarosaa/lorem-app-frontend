import React, { useState, useEffect } from "react";
import api from "./services/api";
import "./App.css";

//import Clipboard from 'react-clipboard';

function App() {
  const [count, setCount] = useState("");
  const [format, setFormat] = useState("");
  const [unit, setUnit] = useState("");
  const [result, setResult] = useState("");

  async function setGeneratedContent(content) {
    setResult(content);
  }

  async function handleSumbit(ev) {
    ev.preventDefault();

    const response = await api.post("/content", { count, unit, format });

    setGeneratedContent(response.data);

    document.querySelector(".result").scrollIntoView({
      behavior: "smooth",
    });
  }

  return (
    <div className="App">
      <div className="title-continer">
        <div>Just another</div>
        <h1>Lorem Ipsum Generetor</h1>
      </div>

      <div className="content">
        <form onSubmit={handleSumbit}>
          <label> Do you want... </label>
          <div className="container-input">
            <div className="check-option">
              <input
                type="radio"
                name="unit"
                id="unitP"
                value="paragraphs"
                onChange={(event) => setUnit(event.target.value)}
              />
              <div className="option-bg"></div>
              <label htmlFor="unitP">Paragraphs</label>
            </div>
            <div className="check-option">
              <label htmlFor="unitS">Sentences</label>
              <input
                type="radio"
                name="unit"
                id="unitS"
                value="sentences"
                onChange={(event) => setUnit(event.target.value)}
              />
              <div className="option-bg"></div>
            </div>

            <div className="check-option">
              <label htmlFor="unitW">Words</label>
              <input
                type="radio"
                name="unit"
                id="unitW"
                value="words"
                onChange={(event) => setUnit(event.target.value)}
              />
              <div className="option-bg"></div>
            </div>
          </div>

          <label>How many? </label>
          <div className="container-input">
            <input
              type="number"
              name="count"
              id="count"
              placeholder="Count"
              onChange={(event) => setCount(event.target.value)}
            />
          </div>

          <div className="container-input">
            <button type="submit">Generate paragraphs</button>
          </div>
        </form>

        <div className="result" id="result">
          {result}
        </div>
      </div>
    </div>
  );
}

export default App;
